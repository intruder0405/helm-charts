# helm-charts

Helm charts for deploying radio-browser into k8s

## radiobrowser-api

This chart installs [radiobrowser-api](https://github.com/segler-alex/radiobrowser-api-rust) with optional mysql database and redis cache. radiobrowser-api is the project behind the [radio-browser project](https://www.radio-browser.info/).

## TL;DR

```bash
$ helm repo add rb https://helm.radio-browser.info
$ helm install my-release rb/radiobrowser-api
```

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```

## Parameters

The following tables lists the configurable parameters of the Apache chart and their default values.

Please look at values.yml for parameters.

## Production example
```bash
$ cat << EOF > mysql-values.yml
auth:
  database: "radio"
  rootPassword: "rootpassword"
  username: "radiouser"
  password: "password"
EOF
$ helm install mysql bitnami/mysql -f mysql-values.yml
$ cat << EOF > redis-values.yml
usePassword: false
master:
  persistence:
    enabled: false
cluster:
  enabled: false
EOF
$ helm install redis bitnami/redis -f redis-values.yml
$ cat << EOF > rb-values.yml
check:
  enabled: true

ingress:
  enabled: true
  hosts:
    - host: my.host.name

mysql:
  enabled: false
  remoteurl: "mysql://radiouser:password@mysql/radio"

redis:
  enabled: false
  remoteurl: "redis://redis-master:6379"
EOF
$ helm install rb rb/radiobrowser-api -f rb-values.yml
```